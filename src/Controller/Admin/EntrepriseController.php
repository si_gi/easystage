<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class EntrepriseController extends AbstractController
{
    /**
     * @Route("/entreprise/", name="entreprise")
     */
    public function findAllEntreprise()
    {
        $Entreprises = $this->getDoctrine()->getRepository(Entreprise::class)->findAll();
        return $this->render('Entreprise/index.html.twig', compact('Entreprises'));
    }

    /**
     * @Route("/enterprise/new", name="newEntreprise")
     */
    public function newEntreprise(Request $request){
        $entreprise = new Entreprise();

        $form = $this->createFormBuilder($entreprise)
            ->add('nomEntreprise', TextType::class)
            ->add('adresse', TextType::class)
            ->add('codePostal', NumberType::class)
            ->add('ville', TextType::class)
            ->add('secteurActivite', ChoiceType::class, array(
                'choices' => array(
                    'Enseignement' => 'Enseignement',
                    'Commerce/Distribution' => 'Commerce/Distribution',
                    'Hôtellerie/Restauration' => 'Hôtellerie/Restauration',
                    'Droit/Justice' => 'Droit/Justice',
                    'Multimedia/Audiovisuel/Informatique' => 'Multimedia/Audiovisuel/Informatique',
                    'Sciences humaines' => 'Sciences humaines',
                    'Industrie' => 'Industrie',
                    'Transports/Automobile' => 'Transports/Automobile',
                    'Bâtiment/Construction' => 'Bâtiment/Construction',
                    'Banque/Assurance' => 'Banque/Assurance',
                    'Logistique' => 'Logistique',
                    'Fonction publique' => 'Fonction publique',
                    'Santé' => 'Santé',
                    'Art/Spectacle' => 'Art/Spectacle',
                    'Sport' => 'Sport',
                    'Autre' => 'Autre',
                )
            ))
            ->add('numero', TextType::class)
            ->add('mail', TextType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($entreprise);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('Entreprise/newEntreprise.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
