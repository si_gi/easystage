<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdministrateurController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/users", name="adminShowUsers")
     */
    public function showUsers(){

        # get all users
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();

        return $this->render('admin/showUsers.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/users/delete/{id}", name="adminDeleteUser")
     */
    public function deleteUser($id){

        # Users entity manager
        $userEntityManager = $this->getDoctrine()->getManager();

        # try to retrieve a user with the ip passed in the url

        $user = $this->getDoctrine()->getRepository(Users::class)->find($id);

        if ($user != null){
            # Delete
            $userEntityManager->remove($user);
            $userEntityManager->flush();
            $this->addFlash('success', "User removed succesfully !");

            return $this->redirectToRoute("adminShowUsers");
        }else{
            $this->addFlash('warning', "Can't find the user you wanted to delete, try with another one !");
            return $this->redirectToRoute("adminShowUsers");
        }
    }

    /**
     * @Route("/admin/user/edit/{id}", name="user_edit")
     */
    public function edit($id, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(Users::class)->find($id);
        $form = $this->createFormBuilder($user)
            ->add('login', TextType::class, array('label' => "Login"))
            ->add('name', TextType::class, array('label' => "Name :"))
            ->add('lastname', TextType::class, array('label' => "Lastname :"))
            ->add('plainpassword', PasswordType::class, array('label' => "Password :"))
            ->add('type', ChoiceType::class, array("choices" => array("Student" => "student", "Professor" => "professor", "Admin" => "admin")  , "mapped" => false))
            ->add('create', SubmitType::class, array("label" => "Edit"))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $encodedNewPassword = $passwordEncoder->encodePassword($this->getUser(), $form->get('plainpassword')->getData());
            $entityManager = $this->getDoctrine()->getManager();

            $userRepo = $this->getDoctrine()->getRepository(Users::class);
            $isLoginAvaible = $userRepo->getUsersPerLogin($form->get('login')->getData());
            # If the login is not available we redirect to inscription page
            if(count($isLoginAvaible) > 0 && $form->get('login')->getData() != $this->getUser()->getLogin()) {
                $this->addFlash('warning', "Sorry, this login is already used by someone else !");
                return $this->redirectToRoute('adminShowUsers');
            }
            $user->setPassword($encodedNewPassword);
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('adminShowUsers');
        }
        return $this->render('admin/edit.html.twig', array(
            'form' => $form->createView()));
    }

    /**
     * @Route("/liste/eleve", name="listeEleve")
     */
    public function listeEleve()
    {
        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleve = $repository->findAll();

        return $this->render('easy/listeEleve.html.twig', compact("eleve"));
    }

}

