<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Eleve;
use App\Entity\Professeur;
use App\Entity\Stage;
//use App\Entity\Tuteur;
use Symfony\Component\HttpFoundation\Request;

class ProfesseurController extends AbstractController
{
    /**
     * @Route("/professeur", name="professeur")
     */
    public function index()
    {
        //recuperer le nombre d'eleve ayant un stage
        //recuperer une liste contenant les eleves ayant un stage
        //recuperer le nombre d'eleve n'ayant pas de stage
        //recuperer une liste contenant les eleves n'ayant pas de stage
        return $this->render('professeur/index.html.twig', [
            'controller_name' => 'ProfesseurController',
        ]);
    }
    /**
     * @Route("/liste/eleve", name="listeEleve")
     */
    public function listeEleve()
    {
        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleve = $repository->findAll();

        return $this->render('easy/listeEleve.html.twig', compact("eleve"));
    }


    /**
     * @Route("/liststage", name="list_stage")
     */
    public function getStage(){
        $eleve = $this->getDoctrine()
            ->getRepository(Eleve::class)
            ->findAll();
        //$stages = $eleve ->getStage();

        return $this->render('professeur/listStage.html.twig', array('list'=>$eleve));
    }

    /**
     * @Route("/add/stage", name="addStage")
     */
    public function AddStage(Request $request)
    {

        $stage = new Stage();

        $stage = $this->createFormBuilder($stage)
            ->add('eleve', TextType::class)
            ->add('entreprise',TextType::class)
            ->add('prof', TextType::class, array('label' => 'prof','required' => false))
            ->add('horairedebut', TextType::class)
            ->add('horairefin', TextType::class)
            ->add('dateexecution', BirthdayType::class)
            ->add('Ajouter', SubmitType::class, array('label' => 'Ajouter un stage'))
            ->getForm();

        $stage->handleRequest($request);

        if ($stage->isSubmitted() && $stage->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $stage = $stage->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stage);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('stage/addStage.html.twig', array(
            'form' => $stage->createView(),
        ));
    }

}
