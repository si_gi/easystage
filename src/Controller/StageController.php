<?php

namespace App\Controller;

use App\Entity\Eleve;
use App\Entity\Professeur;
use App\Entity\Stage;
use App\Entity\Tuteur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Routing\Annotation\Route;

class StageController extends AbstractController
{
    /**
     * @Route("/stage/new", name="newStage")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $userClass = get_class($user);

        $stage = new Stage();
        $formbuild = $this->createFormBuilder($stage);

        if ($userClass == 'Eleve'){
            $formbuild->add('eleve', ChoiceType::class, array(
                'choices' => array(
                    $user->toString() => $user
                )
            ));
        } else {
            $eleves = $this->getDoctrine()->getRepository(Eleve::class)->findAll();
            $elevesTab = array();

            foreach ($eleves as $eleve){
                $elevesTab[$eleve->toString()] = $eleve;
            }

            $formbuild->add('eleve', ChoiceType::class, array(
                'choices' => $elevesTab
            ));
        }

        $professeurs = $this->getDoctrine()->getRepository(Professeur::class)->findAll();
        $professeursTab = array();

        foreach ($professeurs as $professeur){
            $professeursTab[$professeur->toString()] = $professeur;
        }

        $formbuild->add('professeur', ChoiceType::class, array(
            'choices' => $professeursTab
        ));


        $tuteurs = $this->getDoctrine()->getRepository(Tuteur::class)->findAll();
        $tuteursTab = array();

        foreach ($tuteurs as $tuteur){
            $tuteursTab[$tuteur->toString()] = $tuteur;
        }

        $formbuild->add('tuteur', ChoiceType::class, array(
            'choices' => $tuteursTab
        ));


        $form = $formbuild->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $stage = $form->getData();

            $this->getDoctrine()->getManager()->persist($stage);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('index');
        }



        return $this->render('stage/stageForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/stage/all", name="allStages")
     */
    public function allStages(){
        $stages = $this->getDoctrine()->getRepository(Stage::class)->findAll();

        return $this->render('stage/index.html.twig', array(
            'stages' => $stages
        ));
    }

    /**
     * @Route("/stage/edit/{id}", name="editStage")
     */
    public function editStage($id, Request $request){
        $stage = $this->getDoctrine()->getRepository(Stage::class)->find($id);

        $eleves = $this->getDoctrine()->getRepository(Eleve::class)->findAll();
        $eleveTab = array();
        foreach ($eleves as $eleve)
            $eleveTab[$eleve->toString()] = $eleve;

        $professeurs = $this->getDoctrine()->getRepository(Professeur::class)->findAll();
        $professeurTab = array();
        foreach ($professeurs as $professeur)
            $professeurTab[$professeur->toString()] = $professeur;

        $tuteurs = $this->getDoctrine()->getRepository(Tuteur::class)->findAll();
        $tuteurTab = array();
        foreach ($tuteurs as $tuteur)
            $tuteurTab[$tuteur->toString()] = $tuteur;

        $form = $this->createFormBuilder($stage)
            ->add('eleve', ChoiceType::class, array(
                'choices' => $eleveTab
            ))
            -> add('professeur', ChoiceType::class, array(
                'choices' => $professeurTab
            ))
            ->add('tuteur', ChoiceType::class, array(
                'choices' => $tuteurTab
            ))
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $stage = $form->getData();

            $this->getDoctrine()->getManager()->persist($stage);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('stage/stageForm.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/stage/delete/{id}", name="deleteStage")
     */
    public function deleteStage($id){
        $stage = $this->getDoctrine()->getRepository(Stage::class)->find($id);

        $this->getDoctrine()->getManager()->remove($stage);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('allStages');
    }
}
