<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 04/11/2018
 * Time: 14:49
 */

namespace App\Controller;

use App\Entity\Eleve;
use App\Entity\Entreprise;
use App\Entity\Professeur;
use App\Entity\Tuteur;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function update(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();

        $userClass = get_class($user);
        if ($userClass == Eleve::class)
            $type = "Eleve";
        elseif ($userClass == Professeur::class)
            $type = "Professeur";
        elseif ($userClass == Tuteur::class)
            $type = "Tuteur";

        $form = $this->createFormBuilder($user)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('mail', TextType::class)
            ->add('login', TextType::class)
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password')
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $updatedUser = $form->getData();
            $updatedUser->setPassword($encoder->encodePassword($updatedUser, $updatedUser->getPassword()));


            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('utilisateur/index.html.twig', [
            'form' => $form->createView(),
            'type' => $type
        ]);
    }


}