<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 18/10/2018
 * Time: 10:05
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Entreprise;


class StageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder
                ->add('tuteur', TextType::class)
                ->add('entreprise', EntityType::class, array(
                'class' => Entreprise::class,
                'choice_label' => 'nom',
            ))
            ->add('submit', SubmitType::class, ['label'=>'Envoyer', 'attr'=>['class'=>'btn-primary btn-block']]);

    }}
