<?php

namespace App\Repository;

use App\Entity\ReferenceProfesseur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferenceProfesseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferenceProfesseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferenceProfesseur[]    findAll()
 * @method ReferenceProfesseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferenceProfesseurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferenceProfesseur::class);
    }

//    /**
//     * @return ReferenceProfesseur[] Returns an array of ReferenceProfesseur objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReferenceProfesseur
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
