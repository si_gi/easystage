<?php

namespace App\Repository;

use App\Entity\TableReference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TableReference|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableReference|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableReference[]    findAll()
 * @method TableReference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableReferenceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TableReference::class);
    }

//    /**
//     * @return TableReference[] Returns an array of TableReference objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TableReference
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
